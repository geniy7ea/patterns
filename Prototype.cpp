#include <iostream>
#include <vector>


// ���������� �������� Prototype � ������� ����������� ������ - ������� 
//   �� ������� ���������� ����� ��� ������� ��������� "���������� �����"



// �������� ������� ������� ����������
// ����������� ������� �����
class Warrior
{
public:
    virtual Warrior* clone() = 0;
    virtual void info() = 0;
    virtual ~Warrior() {}
};


// ����������� ������ ��������� ����� �����
class Infantryman : public Warrior
{
    friend class PrototypeFactory;
public:
    Warrior* clone() {
        return new Infantryman(*this);
    }
    void info() {
        std::cout << "Infantryman" << std::endl;
    }
private:
    Infantryman() {}
};

class Archer : public Warrior
{
    friend class PrototypeFactory;
public:
    Warrior* clone() {
        return new Archer(*this);
    }
    void info() {
        std::cout << "Archer" << std::endl;
    }
private:
    Archer() {}
};

class Horseman : public Warrior
{
    friend class PrototypeFactory;
public:
    Warrior* clone() {
        return new Horseman(*this);
    }
    void info() {
        std::cout << "Horseman" << std::endl;
    }
private:
    Horseman() {}
};


// ������� ��� �������� ������ ������ ���� ����� �����
class PrototypeFactory
{
public:
    Warrior* createInfantrman() {
        static Infantryman prototype;
        return prototype.clone();
    }
    Warrior* createArcher() {
        static Archer prototype;
        return prototype.clone();
    }
    Warrior* createHorseman() {
        static Horseman prototype;
        return prototype.clone();
    }
};


//int main()
//{
//    PrototypeFactory factory;
//    std::vector<Warrior*> v;
//    v.push_back(factory.createInfantrman());
//    v.push_back(factory.createArcher());
//    v.push_back(factory.createHorseman());
//
//    for (int i = 0; i < v.size(); i++)
//        v[i]->info();
//    // ...
//}